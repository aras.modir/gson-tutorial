package com.aras.modir.gsontutorial;


import com.google.gson.annotations.Expose;

public class EmployeeOne {

    @Expose
    private String firstName;

    @Expose(serialize = false)
    private int age;

    @Expose(deserialize = false)
    private String mail;

    private String password;


    public EmployeeOne(String firstName, int age, String mail, String password) {
        this.firstName = firstName;
        this.age = age;
        this.mail = mail;
        this.password = password;
    }
}
