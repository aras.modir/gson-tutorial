package com.aras.modir.gsontutorial;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Gson gson = new GsonBuilder().excludeFieldsWithoutExposeAnnotation().create();

        EmployeeOne employeeOne = new EmployeeOne(
                "John",
                30,
                "john@gmail.com",
                "asdqwe"
        );

        String jsonResults = gson.toJson(employeeOne);



        /*
        Address address = new Address("Germany", "Berlin");

        List<FamilyMember> familyMembers = new ArrayList<>();
        familyMembers.add(new FamilyMember("wife", 30));
        familyMembers.add(new FamilyMember("Daughter", 5));

        Employee employee = new Employee("John", 30, "john@gmail.com", address, familyMembers);
        String json = gson.toJson(familyMembers);
        */

        /*
        String json = "{\"address\":{\"city\":\"Berlin\",\"country\":\"Germany\"},\"age\":30,\"family\":[{\"age\":30,\"role\":\"wife\"},{\"age\":5,\"role\":\"Daughter\"}],\"first_name\":\"John\",\"mail\":\"john@gmail.com\"}";
//        Employee employee = gson.fromJson(json, Employee.class);
//        FamilyMember[] family = gson.fromJson(json, FamilyMember[].class);

        Type familyType = new TypeToken<ArrayList<FamilyMember>>() {}.getType();
        ArrayList<FamilyMember> family = gson.fromJson(json, familyType);
        */

    }
}
